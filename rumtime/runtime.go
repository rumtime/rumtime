package rumtime

import (
	"bytes"
	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/client"
	"golang.org/x/net/context"
	"io/ioutil"
	"os"
	"path"
)

type Language struct {
	Image     string
	Extension string
	RunCmd    []string
}

var PROGRAM_HOST_DIR = "/tmp/rumtime"
var PROGRAM_CONTAINER_DIR = "/sandbox"

var ALLOWED_LANGS = map[string]*Language{
	"go":      &Language{"golang:1.13.1-alpine", "*.go", []string{"go", "run"}},
	"python3": &Language{"python:3.7.4-alpine", "*.py", []string{"python"}},
}

type RuntimeContext struct {
	Language    string
	ProgramPath string
}

func Run(langId string, code []byte) (string, error) {
	lang := ALLOWED_LANGS[langId]
	tmpfile, err := ioutil.TempFile("tmp", lang.Extension)
	if err != nil {
		return "", err
	}

	if _, err := tmpfile.Write(code); err != nil {
		tmpfile.Close()
		return "", err
	}
	tmpfile.Close()

	ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		return "", err
	}

	runCmd := make([]string, len(lang.RunCmd))
	copy(runCmd, lang.RunCmd)
	runCmd = append(runCmd, path.Join(PROGRAM_CONTAINER_DIR, path.Base(tmpfile.Name())))

	resp, err := cli.ContainerCreate(ctx, &container.Config{
		Image:           lang.Image,
		Cmd:             runCmd,
		AttachStdout:    true,
		AttachStderr:    true,
		NetworkDisabled: true,
		Tty:             true,
	},
		&container.HostConfig{
			Mounts: []mount.Mount{
				{
					Type:   mount.TypeBind,
					Source: PROGRAM_HOST_DIR,
					Target: PROGRAM_CONTAINER_DIR,
				},
			},
		},
		nil, "")
	if err != nil {
		return "", err
	}

	if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		return "", err
	}

	statusCh, errCh := cli.ContainerWait(ctx, resp.ID, container.WaitConditionNotRunning)
	select {
	case err := <-errCh:
		if err != nil {
			return "", err
		}
	case <-statusCh:
	}

	options := types.ContainerLogsOptions{ShowStdout: true}
	reader, err := cli.ContainerLogs(ctx, resp.ID, options)
	if err != nil {
		return "", err
	}
	defer reader.Close()

	if err := cli.ContainerRemove(ctx, resp.ID, types.ContainerRemoveOptions{}); err != nil {
		return "", err
	}
	os.Remove(tmpfile.Name())

	buf := new(bytes.Buffer)
	buf.ReadFrom(reader)
	s := buf.String()
	return s, nil

}
